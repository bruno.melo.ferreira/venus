package org.academiadecodigo.tailormoons.placeholder.controllers.web;

import org.academiadecodigo.tailormoons.placeholder.converters.CustomerDtoToCustomer;
import org.academiadecodigo.tailormoons.placeholder.converters.CustomertoCustomerDto;
import org.academiadecodigo.tailormoons.placeholder.dto.CustomerDto;
import org.academiadecodigo.tailormoons.placeholder.exceptions.UserNotFoundException;
import org.academiadecodigo.tailormoons.placeholder.persistence.model.Customer;
import org.academiadecodigo.tailormoons.placeholder.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class MainController {

    //services
    //dto
    private CustomerService customerService;
    private CustomertoCustomerDto customertoCustomerDto;
    private CustomerDtoToCustomer customerDtoToCustomer;

    //setters
    @Autowired
    public void setCustomertoCustomerDto(CustomertoCustomerDto customertoCustomerDto) {
        this.customertoCustomerDto = customertoCustomerDto;
    }

    @Autowired
    public void setCustomerDtoToCustomer(CustomerDtoToCustomer customerDtoToCustomer) {
        this.customerDtoToCustomer = customerDtoToCustomer;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

   // @RequestMapping("/")
    public String home() {
        return "index";
    }

   // @RequestMapping("/signup")
    public ModelAndView signUp () {

        CustomerDto customerDto = customertoCustomerDto.convert(new Customer());

        return new ModelAndView("signup", "userDto", customerDto);
    }

   // @PostMapping("/signup")
    public String addUser(@Valid @ModelAttribute CustomerDto customerDto, BindingResult bindingResult) throws UserNotFoundException {

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        Customer customer = customerDtoToCustomer.convert(customerDto);

        customerService.save(customer);

        return "redirect:/" + customer.getId();
    }
}
