package org.academiadecodigo.tailormoons.placeholder.controllers.web;

import org.academiadecodigo.tailormoons.placeholder.converters.CustomerDtoToCustomer;
import org.academiadecodigo.tailormoons.placeholder.converters.CustomertoCustomerDto;
import org.academiadecodigo.tailormoons.placeholder.dto.CustomerDto;
import org.academiadecodigo.tailormoons.placeholder.exceptions.UserNotFoundException;
import org.academiadecodigo.tailormoons.placeholder.persistence.model.Customer;
import org.academiadecodigo.tailormoons.placeholder.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
public class UserController {


    private CustomerService customerService;
    //DTOs

    private CustomerDtoToCustomer customerDtoToCustomer;
    private CustomertoCustomerDto customertoCustomerDto;


    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCustomerDtoToCustomer(CustomerDtoToCustomer customerDtoToCustomer) {
        this.customerDtoToCustomer = customerDtoToCustomer;
    }

    @Autowired
    public void setCustomertoCustomerDto(CustomertoCustomerDto customertoCustomerDto) {
        this.customertoCustomerDto = customertoCustomerDto;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public String showUser(@PathVariable Integer id, Model model) throws UserNotFoundException {

        Customer customer = customerService.get(id);

        model.addAttribute("user", customertoCustomerDto.convert(customer));

        return "user/show";
    }

    @GetMapping("/{id}/submit-video")
    public String showFormToSubmitVideos(@PathVariable Integer id, Model model) throws UserNotFoundException{

        Customer customer = customerService.get(id);

        CustomerDto customerDto = customertoCustomerDto.convert(customer);

        model.addAttribute("user", customerDto);

        return "video-form"; // Review
    }


}
